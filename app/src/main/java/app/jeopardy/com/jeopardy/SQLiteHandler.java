package app.jeopardy.com.jeopardy;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * created by ekirapa
 */
public class SQLiteHandler extends SQLiteOpenHelper {
    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "DB_TooDo";

    // Table name
    private static final String TABLE_CATEGORIES = "Categories";
    private static final String TABLE_CONTENT = "Content";
    private static final String TABLE_MYTOPICS = "MyTopics";
    private static final String TABLE_USERs = "Users";
    // Login Table Columns names
    private static final String KEY_NAME = "Name";
    private static final String KEY_EMAIL = "Email";
    private static final String KEY_PASSWORD = "Password";
    private static final String KEY_ID = "Id";
    private static final String KEY_STATUS = "Status";
    private static final String KEY_QUESTION = "Question";
    private static final String KEY_ANSWER = "Answer";
    private static final String KEY_CATEGORY = "Category";
    private static final String KEY_GENDER = "Gender";
    private static final String KEY_RESOURCE = "Resource";
    private static final String KEY_COMMENT = "Comment";
    private static final String TABLE_ANSWERS = "Answers";
    private static final String KEY_QID = "QuestionId";


    Context mconContext;


    public SQLiteHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.mconContext = context;

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    private long addContent(String question, String category) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_QUESTION, question);
        values.put(KEY_CATEGORY, category);
        long rowid = db.insert(TABLE_CONTENT, null, values);
        db.close();
        return rowid;
    }

    private void addAnswer(String answer, long qid) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ANSWER, answer);
        values.put(KEY_QID, qid);
        db.insert(TABLE_ANSWERS, null, values);
        db.close();
    }

    public JSONArray getContent(String topic) {
        JSONArray project_array = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String query;
        query = "SELECT * FROM " + TABLE_CONTENT + " WHERE " + KEY_CATEGORY + " ='" + topic + "'";
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {

            int i = 0;
            do {
                JSONObject obj = new JSONObject();
                try {
                    obj.put(KEY_ID, cursor.getInt(0));
                    obj.put(KEY_QUESTION, cursor.getString(1));
                    obj.put(KEY_CATEGORY, cursor.getString(2));
                    project_array.put(i, obj);
                    i++;
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return project_array;
    }

    public List<JSONObject> getAnswers(long qid) {
        SQLiteDatabase db = this.getWritableDatabase();
        List<JSONObject> answerlist = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_ANSWERS + " WHERE " + KEY_QID + " = " + qid;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        do {

            try {
                JSONObject object = new JSONObject();
                object.put(KEY_ID, cursor.getInt(0));
                object.put(KEY_ANSWER, cursor.getString(1));
                object.put(KEY_QID, cursor.getInt(2));
                answerlist.add(object);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } while (cursor.moveToNext());
        cursor.close();
        db.close();
        return answerlist;
    }

    public List<JSONObject> getCategory() {
        List<JSONObject> array = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_CATEGORIES;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {

            do {
                JSONObject obj = new JSONObject();
                try {
                    obj.put(KEY_ID, cursor.getInt(0));
                    obj.put(KEY_CATEGORY, cursor.getString(1));
                    obj.put(KEY_RESOURCE, cursor.getString(2));
                    obj.put(KEY_COMMENT, cursor.getString(3));
                    array.add(obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return array;
    }

    public int getCategoryResource(String category) {
        SQLiteDatabase db = this.getWritableDatabase();
        int resid = 0;
        String query = "SELECT * FROM " + TABLE_CATEGORIES + " WHERE " + KEY_CATEGORY + " ='" + category + "'";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        resid = cursor.getInt(2);
        cursor.close();
        db.close();
        return resid;
    }

    public String getCategoryComment(String category) {
        SQLiteDatabase db = this.getWritableDatabase();
        String comment;
        String query = "SELECT * FROM " + TABLE_CATEGORIES + " WHERE " + KEY_CATEGORY + " ='" + category + "'";
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        comment = cursor.getString(3);
        cursor.close();
        db.close();
        return comment;
    }

    public List<String> getMyTopics() {
        List<String> topics = new ArrayList<>();
        SQLiteDatabase db = this.getWritableDatabase();
        String query = "SELECT * FROM " + TABLE_MYTOPICS;
        Cursor cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            do {
                topics.add(cursor.getString(1));
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return topics;
    }

    // Storing category
    public void addCategory(String name, int resource, String comment) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_CATEGORY, name);
        values.put(KEY_RESOURCE, resource);
        values.put(KEY_COMMENT, comment);
        db.insert(TABLE_CATEGORIES, null, values);
        db.close(); // Closing database connection

    }

    public void addMyTopic(String topic) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_CATEGORY, topic);
        db.insert(TABLE_MYTOPICS, null, values);
        db.close(); // Closing database connection

    }

    private void writeCategories() {
        String names[] = {"Sex Education", "True or False", "Myth Busting", "Puberty", "Sexual Hygine", "All about Love"};
        String comments[] = {"Why so shy?", "Fact or fiction", "Have i been wrong all along?", "Mom am growing!", "Maintain good sexual hygine", "Love makes the world go round"};
        int resids[] = {R.mipmap.education, R.mipmap.brain, R.mipmap.myth, R.mipmap.sexuality, R.mipmap.hygine, R.mipmap.love};
        for (int i = 0; i < names.length; i++) {
            addCategory(names[i], resids[i], comments[i]);
        }
    }

    private void writeContent() {
        String topics = "Puberty";
        String questions[] = {"What does puberty entail?", "What are the changes that occur during puberty?", "How do I cope with the changes that come with puberty?", "The following are true about menstrual bleeding except?", "What is PMS?"};
        String answers1[] = {"False-glands in your body produce hormones that cause no changes", "True-starts sometime between the ages of 8 and 12", "False-is an abnormal and unhealthy part of growing up", "False-all of the above"};
        String answers2[] = {"False-you get taller, and lose weight", "False-your appetite decreases", "True-you may feel sexual attraction to others", "False-you get wiser and more clever"};
        String answer3[] = {"True-talk to someone you trust if you have any questions about puberty", "False-dont talk to anyone", "False-be in denial about your body", "False-runaway from home"};
        String answer4[] = {"False-Day 1 to 6 – menstrual bleeding", "False-Day 7 to 12 – uterus lining thickens", "True-Day 13 to 15 – firtilization", "False-Day 16 to 24 – ovulation"};
        String answer5[] = {"True-postmenstrual sysndrome", "False-premenstrual syndrome", "False-prematernal sundrome", "False-postmaternal syndrome"};
        long id1 = addContent(questions[0], topics);
        addAnswer(answers1[0], id1);
        addAnswer(answers1[1], id1);
        addAnswer(answers1[2], id1);
        addAnswer(answers1[3], id1);

        long id2 = addContent(questions[1], topics);
        addAnswer(answers2[0], id2);
        addAnswer(answers2[1], id2);
        addAnswer(answers2[2], id2);
        addAnswer(answers2[3], id2);
        Toast.makeText(mconContext, "Create answer,", Toast.LENGTH_LONG).show();

        long id3 = addContent(questions[2], topics);
        addAnswer(answer3[0], id3);
        addAnswer(answer3[1], id3);
        addAnswer(answer3[2], id3);
        addAnswer(answer3[3], id3);

        long id4 = addContent(questions[3], topics);
        addAnswer(answer4[0], id4);
        addAnswer(answer4[1], id4);
        addAnswer(answer4[2], id4);
        addAnswer(answer4[3], id4);

        long id5 = addContent(questions[4], topics);
        addAnswer(answer5[0], id5);
        addAnswer(answer5[1], id5);
        addAnswer(answer5[2], id5);
        addAnswer(answer5[3], id5);
        String topic2 = "Sex Education";
        String questions2[] = {"Sexuality means the following except?", "Expressing your sexuality is the following except?", "You need to know the following about sex before having sex except?", "What makes a healthy sexual relationship?", "What is birth control?"};
        long rows[] = {0, 0, 0, 0, 0};
        for (int i = 0; i < questions2.length; i++) {
            rows[i] = addContent(questions2[i], topic2);
        }
        String ans1[] = {"False-sexual orientation refers to the sexual attraction you feel towards those of the opposite or\n" +
                "same sex", "False-erotic practices or stimuli are those that arouse sexual desire", "False-sexual behavior or practices refer to actions such as touching, kissing, or other actions that\n" +
                "you do regularly to stimulate yourself or others", "True-It is a gender and not a topic nor an action"};
        String ans2[] = {"False-feel bad/good about how you look", "False-flirt or give compliments", "False-hug, kiss or hold hands to show affection", "True-go to school/work everyday"};
        String ans3[] = {"False-how you feel about each other", "False-whether you are both ready to have sex", "False-what having sex means to each of you", "True-you can be promiscuous about it as you want"};
        String ans4[] = {"False-Secrecy", "False-Promiscucity", "False-Infidelty", "True-honest communication"};
        String ans5[] = {"True-avoiding pregnancy while indulging oin sex", "False-abortion upon conception", "False-Female Genital Mutilation", "False-Gender Equality"};
        for(int j = 0;j<ans1.length;j++){
            addAnswer(ans1[j], rows[0]);
        }
        for(int j = 0;j<ans2.length;j++){
            addAnswer(ans2[j], rows[1]);
        }
        for(int j = 0;j<ans3.length;j++){
            addAnswer(ans3[j], rows[2]);
        }
        for(int j = 0;j<ans4.length;j++){
            addAnswer(ans4[j], rows[3]);
        }
        for(int j = 0;j<ans5.length;j++){
            addAnswer(ans5[j], rows[4]);
        }
    }

    private void writeMine() {
        String names[] = {"Sex Education", "Puberty", "Myth Busting"};
        for (int i = 0; i < names.length; i++) {
            addMyTopic(names[i]);
        }
    }

    public void createDatabase() {
        createCategory();
        createContent();
        createUsersTable();
        createMyTopics();
        createContent();
        createAnswers();
        writeCategories();
        writeMine();
        writeContent();

    }

    //Users
    private void createUsersTable() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERs);
        String colors_query = "CREATE TABLE IF NOT EXISTS " + TABLE_USERs
                + "(" + KEY_ID
                + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL , " + KEY_NAME
                + " TEXT, " + KEY_EMAIL
                + " TEXT, " + KEY_PASSWORD
                + " TEXT, " + KEY_GENDER + " TEXT )";
        db.execSQL(colors_query);
        db.close();
    }

    public void addUser(String username, String email, String password, String gender) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, username);
        values.put(KEY_EMAIL, email);
        values.put(KEY_PASSWORD, password);
        values.put(KEY_GENDER, gender);
        db.insert(TABLE_USERs, null, values);
        db.close();
    }

    public boolean getUser(String username, String password) {
        SQLiteDatabase db = this.getReadableDatabase();
        int count = 0;
        boolean y;
        String query = "SELECT * FROM " + TABLE_USERs + " WHERE " + KEY_NAME + "='" + username + "'" + " AND " + KEY_PASSWORD + "='" + password + "'";
        Cursor cursor = db.rawQuery(query, null);
        count = cursor.getCount();
        cursor.close();
        if (count >= 1) {
            return true;
        } else {
            return false;
        }
    }


    private void createCategory() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CATEGORIES);
        StringBuilder builder = new StringBuilder();
        builder.append("CREATE TABLE IF NOT EXISTS ").append(TABLE_CATEGORIES)
                .append("(").append(KEY_ID)
                .append(" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,")
                .append(KEY_CATEGORY)
                .append(" TEXT ,")
                .append(KEY_RESOURCE)
                .append(" INTEGER ,")
                .append(KEY_COMMENT)
                .append(" TEXT )");
        db.execSQL(builder.toString());
        db.close();
    }

    private void createMyTopics() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MYTOPICS);
        StringBuilder builder = new StringBuilder();
        builder.append("CREATE TABLE IF NOT EXISTS ").append(TABLE_MYTOPICS)
                .append("(").append(KEY_ID)
                .append(" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,")
                .append(KEY_CATEGORY).append(" INTEGER )");
        db.execSQL(builder.toString());
        db.close();
    }

    private void createContent() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTENT);
        StringBuilder builder = new StringBuilder();
        builder.append("CREATE TABLE IF NOT EXISTS ").append(TABLE_CONTENT)
                .append("(").append(KEY_ID)
                .append(" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,")
                .append(KEY_QUESTION).append(" Text ,")
                .append(KEY_CATEGORY)
                .append(" TEXT )");
        db.execSQL(builder.toString());
        db.close();

    }

    private void createAnswers() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ANSWERS);
        StringBuilder builder = new StringBuilder();
        builder.append("CREATE TABLE IF NOT EXISTS ").append(TABLE_ANSWERS)
                .append("(").append(KEY_ID)
                .append(" INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,")
                .append(KEY_ANSWER).append(" Text ,")
                .append(KEY_QID).append(" INTEGER )");
        db.execSQL(builder.toString());
        db.close();

    }

}
