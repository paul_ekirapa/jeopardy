package app.jeopardy.com.jeopardy;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 7/24/2016.
 */
public class Topics extends Fragment implements SearchView.OnQueryTextListener {
    private static int SPAN_SIZE = 3;
    RecyclerView mRecycler;
    TopicsAdapter adapter;
    SQLiteHandler handler;
    private View v;
    private List<JSONObject> filteredlist;
    private List<JSONObject> list = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        v = inflater.inflate(R.layout.topics, container, false);
        if (((MainActivity) getActivity()).getSupportActionBar() != null) {
            ((MainActivity) getActivity()).getSupportActionBar().setTitle("All Topics");
        }
        handler = new SQLiteHandler(getContext());
        setupRecycler();
        return v;

    }

    private void setupRecycler() {
        mRecycler = (RecyclerView) v.findViewById(R.id.alltopicsrecycler);
        list = handler.getCategory();
        adapter = new TopicsAdapter(getContext(), list, false);
        // Check screen size
        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE) {
            SPAN_SIZE = 4;
        }
        // Medium screen devices landscape orientation
        if ((getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_NORMAL) {
            Display display = ((WindowManager) getContext().getSystemService(getActivity().WINDOW_SERVICE))
                    .getDefaultDisplay();
            int orientation = display.getRotation();
            if (orientation == Surface.ROTATION_90
                    || orientation == Surface.ROTATION_270) {
                SPAN_SIZE = 4;
            }
        }
        GridLayoutManager manager = new GridLayoutManager(getContext(), SPAN_SIZE);
        mRecycler.setLayoutManager(manager);
        mRecycler.addOnItemTouchListener(new RecyclerItemClickListener(
                getContext(), new RecyclerItemClickListener.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                JSONObject object = list.get(position);
                try {
                    String name = object.getString("Category");
                    int resid = object.getInt("Resource");
                    String comment = object.getString("Comment");
                    Intent intent = new Intent(getContext(), StartQuiz.class);
                    Bundle b = new Bundle();
                    b.putString("Category", name);
                    b.putInt("Resource", resid);
                    b.putString("Comment", comment);
                    intent.putExtras(b);
                    startActivity(intent);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }));
        mRecycler.setAdapter(adapter);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.search_menu, menu);
        final MenuItem searchItem = menu.findItem(R.id.search);
        final SearchView search = (SearchView) MenuItemCompat
                .getActionView(searchItem);
        search.setOnQueryTextListener(this);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        if (filteredlist.size() == 0) {
            Toast.makeText(getContext(),
                    " Item not found",
                    Toast.LENGTH_LONG).show();
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        List<JSONObject> filteredlist = filter(newText, list);
        adapter.animateTo(filteredlist);
        mRecycler.scrollToPosition(0);
        return true;
    }

    private List<JSONObject> filter(String query, List<JSONObject> objects) {
        filteredlist = new ArrayList<>();
        query = query.toLowerCase();
        if (query.toString().isEmpty()) {
            filteredlist = objects;
            return filteredlist;
        }
        for (int i = 0; i < list.size(); i++) {
            try {
                String flag_name = objects.get(i).getString("Category")
                        .toLowerCase();
                if (flag_name.contains(query)) {
                    filteredlist.add(objects.get(i));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return filteredlist;

    }
}


