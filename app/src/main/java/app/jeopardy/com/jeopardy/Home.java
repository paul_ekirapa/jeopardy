package app.jeopardy.com.jeopardy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 7/24/2016.
 */
public class Home extends Fragment {
    private static final String PREF_NAME = "UserInfo";
    TextView profilename, profilelevel, tv_popularTopics, tv_myTopics;
    List<JSONObject> list = new ArrayList<>();
    List<String> popularList = new ArrayList<>();
    RecyclerView mPopularRecycler, myTopicsRecycler;
    TopicsAdapter adapter;
    MyTopicsAdapter topicsAdapter;
    private View v;
    private SharedPreferences pref;
    private String name, level;
    private SQLiteHandler handler;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.home, container, false);
        handler = new SQLiteHandler(getContext());
        pref = getContext().getSharedPreferences(PREF_NAME, getActivity().MODE_PRIVATE);
        name = pref.getString("Name", "Nameless");
        level = pref.getString("Level", "Novice");
        if (((MainActivity) getActivity()).getSupportActionBar() != null) {
            ((MainActivity) getActivity()).getSupportActionBar().setTitle(getString(R.string.app_name));
        }

        profilename = (TextView) v.findViewById(R.id.profile_name);
        profilelevel = (TextView) v.findViewById(R.id.profile_level);
        tv_popularTopics = (TextView) v.findViewById(R.id.tv_categories);
        tv_myTopics = (TextView) v.findViewById(R.id.tv_my_topics);

        profilename.setText(name);
        profilelevel.setText(level);
        setupMyTopicsRecycler();
        setupPopularRecycler();
        return v;

    }

    private void setupPopularRecycler() {
        list = handler.getCategory();
        adapter = new TopicsAdapter(getContext(), list, false);
        mPopularRecycler = (RecyclerView) v.findViewById(R.id.landing_popular_recycler);
        mPopularRecycler.setLayoutManager(new LinearLayoutManager(
                getContext(), LinearLayoutManager.HORIZONTAL, false));
        mPopularRecycler.addOnItemTouchListener(new RecyclerItemClickListener(
                getContext(),
                new RecyclerItemClickListener.OnItemClickListener() {

                    @Override
                    public void onItemClick(View view, int position) {
                        JSONObject object = list.get(position);
                        try {
                            String name = object.getString("Category");
                            String comment = object.getString("Comment");
                            int resid = object.getInt("Resource");
                            Intent intent = new Intent(getContext(), StartQuiz.class);
                            Bundle b = new Bundle();
                            b.putString("Category", name);
                            b.putInt("Resource", resid);
                            b.putString("Comment",comment);
                            intent.putExtras(b);
                            startActivity(intent);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }));
        mPopularRecycler.setAdapter(adapter);
        tv_popularTopics.setVisibility(View.VISIBLE);
        mPopularRecycler.setVisibility(View.VISIBLE);
    }

    private void setupMyTopicsRecycler() {
        popularList = handler.getMyTopics();
        topicsAdapter = new MyTopicsAdapter(getContext(), popularList);
        myTopicsRecycler = (RecyclerView) v.findViewById(R.id.my_topics_recycler);
        myTopicsRecycler.setLayoutManager(new LinearLayoutManager(
                getContext(), LinearLayoutManager.HORIZONTAL, false));
        myTopicsRecycler.addOnItemTouchListener(new RecyclerItemClickListener(
                getContext(),
                new RecyclerItemClickListener.OnItemClickListener() {

                    @Override
                    public void onItemClick(View view, int position) {
                        String name = popularList.get(position);
                        int resid = handler.getCategoryResource(name);
                        String comment = handler.getCategoryComment(name);
                        Intent intent = new Intent(getContext(), StartQuiz.class);
                        Bundle b = new Bundle();
                        b.putString("Category", name);
                        b.putInt("Resource", resid);
                        b.putString("Comment",comment);
                        intent.putExtras(b);
                        startActivity(intent);
                    }
                }));
        myTopicsRecycler.setAdapter(topicsAdapter);
        tv_myTopics.setVisibility(View.VISIBLE);
        myTopicsRecycler.setVisibility(View.VISIBLE);
    }
}
