package app.jeopardy.com.jeopardy;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.PageTransformer;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.TextView;

public class LoginActivity extends AppCompatActivity implements OnClickListener {

    SessionManager sessionManager;
    Button btn_login, btn_signup;
    ViewPager pager;
    CircleIndicator indicator;
    PagerAdapter adapter;
    int i[] = {R.drawable.back1, R.drawable.back2,
            R.drawable.back3, R.drawable.back4};
    int colors[] = {R.color.red, R.color.orangeish, R.color.brown, R.color.blue};
    String title[] = {"Challenge and Educate Yourself", "Play With your friends and peers",
            "Get Empowered and break myths", "Build a positivee relation with your friends an patner"};

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionManager = new SessionManager(getBaseContext());
        if (sessionManager.isLoggedIn()) {
            Intent in = new Intent(getBaseContext(), MainActivity.class);
            in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(in);
            finish();
        } else {
            setContentView(R.layout.login_splash);

            new SQLiteHandler(getBaseContext()).createDatabase();
            FrameLayout frameLayout = (FrameLayout) findViewById(R.id.frame_layout);
            frameLayout.setAlpha(0.6f);
            pager = (ViewPager) findViewById(R.id.splash_pager);
            btn_login = (Button) findViewById(R.id.btn_login);
            btn_signup = (Button) findViewById(R.id.btn_signup);
            btn_login.setOnClickListener(this);
            btn_signup.setOnClickListener(this);

            setuppager();
        }

    }

    private void setuppager() {
        indicator = (CircleIndicator) findViewById(R.id.splash_indicator);
        adapter = new PagerAdapter(getSupportFragmentManager());

        for (int j = 0; j < i.length; j++) {
            LoginFragment frag = new LoginFragment();
            Bundle args = new Bundle();
            args.putString("Title", title[j]);
            args.putInt("Color", colors[j]);
            args.putInt("Resid", i[j]);
            frag.setArguments(args);
            adapter.addFragment(frag);
        }
        pager.setAdapter(adapter);
        pager.setPageTransformer(true, new PageTransform());
        indicator.setViewPager(pager);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_signup:
                Intent intent = new Intent(getBaseContext(), Signup.class);
                startActivity(intent);
                break;
            case R.id.btn_login:
                Intent in = new Intent(getBaseContext(), Signin.class);
                startActivity(in);
                break;

            default:
                break;
        }

    }

    class PageTransform implements PageTransformer {

        @Override
        public void transformPage(View view, float position) {
            int pageWidth = view.getWidth();

            TextView title = (TextView) view.findViewById(R.id.tv_login_splash);
            if (position <= -1.0F || position >= 1.0F) {
                view.setTranslationX(view.getWidth() * position);
                title.setTranslationX(view.getWidth() * position);
                view.setAlpha(0.0F);
                title.setAlpha(0.0F);

            } else if (position == 0.0F) {
                view.setTranslationX(view.getWidth() * position);
                title.setTranslationX(view.getWidth() * position);
                view.setAlpha(1.0F);
                title.setAlpha(1.0F);
            } else {
                // position is between -1.0F & 0.0F OR 0.0F & 1.0F
                view.setTranslationX(view.getWidth() * -position);
                title.setTranslationX(view.getWidth() * -position);
                view.setAlpha(1.0F - Math.abs(position));
                title.setAlpha(1.0F - Math.abs(position));
            }

        }

    }

}
