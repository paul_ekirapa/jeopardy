package app.jeopardy.com.jeopardy;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;

/**
 * Created by hp on 3/24/2016.
 */
public class Signup extends AppCompatActivity implements View.OnClickListener {
    private EditText names, email, paswsword;
    FrameLayout frameLayout;
    ProgressBar spinner;
    private SharedPreferences pref;
    private static final String PREF_NAME = "UserInfo";
    private RadioGroup radioSexGroup;
    private RadioButton radioSexButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup);
        pref = this.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        frameLayout = (FrameLayout) findViewById(R.id.frame);
        frameLayout.setAlpha(0);
        spinner = (ProgressBar) findViewById(R.id.loading_spinner);
        Toolbar toolbar = (Toolbar) findViewById(R.id.signup_toolbar);
        setSupportActionBar(toolbar);
        findViewById(R.id.btn_submitsignup).setOnClickListener(this);
        names = (EditText) findViewById(R.id.et_first_name);
        email = (EditText) findViewById(R.id.et_email_signUp);
        paswsword = (EditText) findViewById(R.id.et_password_signup);
        radioSexGroup = (RadioGroup) findViewById(R.id.sexGroup);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_submitsignup) {
            if (check()) {
                frameLayout.setVisibility(View.VISIBLE);
                frameLayout.getBackground().setAlpha(240);
                spinner.setVisibility(View.VISIBLE);
//                if (!pref.getString("Name","0User").equals(names.getText().toString())){
//                    new SQLiteHandler(getBaseContext()).createDB();
//                }
                int selectedId = radioSexGroup.getCheckedRadioButtonId();
                radioSexButton = (RadioButton) findViewById(selectedId);
                String gender = radioSexButton.getText().toString();

                new SQLiteHandler(getBaseContext()).addUser(names.getText().toString(), email.getText().toString(), paswsword.getText().toString(),gender);
                Intent in = new Intent(getBaseContext(), MainActivity.class);
                in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                pref.edit().putString("Name", names.getText().toString()).apply();
                pref.edit().putString("Level", "Beginner").apply();

                SharedPreferences pref2 = this.getSharedPreferences("Created",
                        MODE_PRIVATE);
                pref2.edit().putBoolean("Flag", true).apply();
                new SessionManager(getBaseContext()).setLogin(true);
                startActivity(in);
            }
        }
    }

    private boolean check() {
        if (names.getText().toString().isEmpty()) {
            names.setError(getString(R.string.error));
            names.requestFocus();
            return false;
        } else if (email.getText().toString().isEmpty()) {
            email.setError(getString(R.string.error));
            email.requestFocus();
            return false;
        } else if (paswsword.getText().toString().isEmpty()) {
            paswsword.setError(getString(R.string.error));
            paswsword.requestFocus();
            return false;
        }
        return true;

    }

}
