package app.jeopardy.com.jeopardy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.FrameLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 7/26/2016.
 */
public class QuizActivity extends AppCompatActivity implements QuizFrag.verdict, StartFrag.StartGame {
    int fragId = 0;
    int points = 0;
    int right = 0;
    List<QuizFrag> fragList = new ArrayList<>();
    List<JSONObject> answerList = new ArrayList<>();
    JSONArray array = new JSONArray();
    SQLiteHandler handler;
    String topic;
    FrameLayout frame;
    private FragmentTransaction transaction;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.quiz_activity);
        handler = new SQLiteHandler(getBaseContext());
        frame = (FrameLayout) findViewById(R.id.quizframe);
        frame.setAlpha(0.8f);
        topic = getIntent().getExtras().getString("Topic");
        array = handler.getContent(topic);
        setup();


        StartFrag startFrag = new StartFrag();
        Bundle b = new Bundle();
        b.putInt("Resource", getIntent().getExtras().getInt("Resource"));
        b.putString("Topic", topic);
        startFrag.setArguments(b);

        transaction = getSupportFragmentManager().beginTransaction();
        if (array.length() > 0) {
            transaction.add(R.id.quizframe, startFrag).commit();
        }

    }

    private void setup() {
        for (int i = 0; i < array.length(); i++) {
            try {
                JSONObject obj = array.getJSONObject(i);
                Bundle b = new Bundle();
                String question = obj.getString("Question");
                long rowId = obj.getInt("Id");
                b.putString("Question", question);
                List<JSONObject> answrList = handler.getAnswers(rowId);
                ArrayList<String> answers = new ArrayList<>();
                for (int j = 0; j < answrList.size(); j++) {
                    JSONObject jsonObject = answrList.get(j);
                    answers.add(jsonObject.getString("Answer"));
                }
                b.putStringArrayList("Answers", answers);
                b.putInt("Round", i);
                QuizFrag frag = new QuizFrag();
                frag.setArguments(b);
                fragList.add(frag);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void submitVerdict(int grade) {
        fragId++;
        if (grade == 1) {
            right++;
        }
        android.os.Handler handler1 = new android.os.Handler();
        handler1.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (fragId < fragList.size()) {
                    transaction = getSupportFragmentManager().beginTransaction();
                    transaction.replace(R.id.quizframe, fragList.get(fragId)).commit();
                } else {
                    transaction = getSupportFragmentManager().beginTransaction();
                    ResultsFrag frag = new ResultsFrag();
                    Bundle b = new Bundle();
                    b.putInt("Right", right);
                    frag.setArguments(b);
                    transaction.replace(R.id.quizframe, frag).commit();
                }
            }
        }, 2000);


    }

    @Override
    public void startMyGame() {
        Log.d("CLICKED","CLICKED");
        transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.quizframe, fragList.get(fragId)).commit();

    }
}
