package app.jeopardy.com.jeopardy;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by hp on 7/25/2016.
 */
public class StartQuiz extends AppCompatActivity implements View.OnClickListener {
    ImageView imageView;
    TextView quiztitle, quizComment;
    int resource;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.start_quiz);
        ScrollView scrollView = (ScrollView) findViewById(R.id.scroll);
        scrollView.setAlpha(0.8f);
        imageView = (ImageView) findViewById(R.id.quizIcon);
        quizComment = (TextView) findViewById(R.id.tv_quizComment);
        quiztitle = (TextView) findViewById(R.id.tv_quiztitle);

        Bundle b = getIntent().getExtras();
        resource = b.getInt("Resource");
        Picasso.with(getBaseContext()).load(resource).into(imageView);
        quiztitle.setText(b.getString("Category"));
        quizComment.setText(b.getString("Comment"));
        findViewById(R.id.singleplayer).setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.singleplayer:
                Intent intent = new Intent(getBaseContext(), QuizActivity.class);
                Bundle b = new Bundle();
                b.putString("Topic", quiztitle.getText().toString());
                b.putInt("Resource", resource);
                intent.putExtras(b);
                startActivity(intent);
                break;
        }
    }
}
