package app.jeopardy.com.jeopardy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by hp on 7/26/2016.
 */
public class ResultsFrag extends Fragment {
    View v;
    private TextView outOf, tv_points;

    public ResultsFrag() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.results_frag, container, false);
        Bundle b = getArguments();
        int right = b.getInt("Right");
        int points = right * 5;
        outOf = (TextView) v.findViewById(R.id.tv_outOf);
        tv_points = (TextView) v.findViewById(R.id.tv_points);
        outOf.setText("You Got " + right + "/5");
        tv_points.setText("" + points + " Points");
        return v;
    }
}
