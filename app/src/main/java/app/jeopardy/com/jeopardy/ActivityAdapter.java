package app.jeopardy.com.jeopardy;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 7/24/2016.
 */
public class ActivityAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    List<Object> list = new ArrayList<>();
    Context context;

    public ActivityAdapter(Context context) {
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == 1) {
            View v = LayoutInflater.from(context).inflate(R.layout.no_activity_layout, parent, false);
            return new NoActivityHolder(v);
        } else {
            View v = LayoutInflater.from(context).inflate(R.layout.no_activity_layout, parent, false);
            return new ActivityHolder(v);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (getItemCount() == 1) {
            return 1;
        } else {
            return 0;
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (getItemViewType(position) == 1) {
            NoActivityHolder holder1 = (NoActivityHolder) holder;
        } else {
            ActivityHolder holder1 = (ActivityHolder) holder;
        }
    }


    @Override
    public int getItemCount() {
        return list.size() + 1;
    }

    class ActivityHolder extends RecyclerView.ViewHolder {
        public ActivityHolder(View itemView) {
            super(itemView);
        }
    }

    class NoActivityHolder extends RecyclerView.ViewHolder {
        public NoActivityHolder(View itemView) {
            super(itemView);
        }
    }
}
