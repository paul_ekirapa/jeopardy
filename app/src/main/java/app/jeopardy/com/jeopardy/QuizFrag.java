package app.jeopardy.com.jeopardy;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 7/26/2016.
 */
public class QuizFrag extends Fragment implements View.OnClickListener {
    Button option1, option2, option3, option4;
    Bundle b;
    int answerId;
    List<String[]> answerlist = new ArrayList<>();
    List<Button> buttonlist = new ArrayList<>();
    TextView tv_question, tvRound;
    private View v;
    CountDownTimer countDownTimer;

    public QuizFrag() {

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.question_frag, container, false);
        b = getArguments();
        String question = b.getString("Question");
        int round = b.getInt("Round") + 1;
        final LinearLayout linearLayout = (LinearLayout) v.findViewById(R.id.qLinear);

        tvRound = (TextView) v.findViewById(R.id.qRound);
        tvRound.setText("Question " + round + " of 5");

        final TextView mTextField = (TextView) v.findViewById(R.id.tv_timer);
        Animation myFadeOutAnimation = AnimationUtils.loadAnimation(getContext(), R.anim.fadein);
        myFadeOutAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                linearLayout.setVisibility(View.VISIBLE);
                tvRound.setVisibility(View.INVISIBLE);

                 countDownTimer = new CountDownTimer(20000, 1000) {

                    public void onTick(long millisUntilFinished) {
                        mTextField.setText("" + millisUntilFinished / 1000);
                    }

                    public void onFinish() {
                        submit();
                    }
                }.start();

            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        tvRound.setAnimation(myFadeOutAnimation);


        tv_question = (TextView) v.findViewById(R.id.tv_question);
        tv_question.setText(question);
        option1 = (Button) v.findViewById(R.id.option1);
        option3 = (Button) v.findViewById(R.id.option3);
        option2 = (Button) v.findViewById(R.id.option2);
        option4 = (Button) v.findViewById(R.id.option4);
        buttonlist.add(option1);
        buttonlist.add(option2);
        buttonlist.add(option3);
        buttonlist.add(option4);
        for (Button button : buttonlist) {
            button.setOnClickListener(this);
        }

        decode();
        return v;
    }

    private void submit() {
        verdict gradeVerdit = (verdict) getActivity();
        gradeVerdit.submitVerdict(0);
    }

    private void decode() {
        ArrayList<String> answers = new ArrayList<>();
        answers = b.getStringArrayList("Answers");

        for (int j = 0; j < answers.size(); j++) {
            String mAnswerFromList = answers.get(j);
            String answer[] = mAnswerFromList.split("-");
            answerlist.add(answer);

            if (answer[0].equals("True")) {
                answerId = j;
            }
            buttonlist.get(j).setText(answer[1]);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.option1:

                grade(0);
                break;
            case R.id.option2:
                grade(1);
                break;
            case R.id.option3:
                grade(2);
                break;
            case R.id.option4:
                grade(3);
                break;
        }
    }

    private void grade(int position) {
        countDownTimer.cancel();
        int grade = 1;
        if (answerId == position) {
            buttonlist.get(position).setBackgroundColor(ContextCompat.getColor(getContext(), R.color.green));
        } else {
            buttonlist.get(position).setBackgroundColor(ContextCompat.getColor(getContext(), R.color.red));
            buttonlist.get(answerId).setBackgroundColor(ContextCompat.getColor(getContext(), R.color.green));
            grade = 0;
        }
        verdict gradeVerdit = (verdict) getActivity();
        gradeVerdit.submitVerdict(grade);
    }

    public interface verdict {
        void submitVerdict(int grade);
    }
}
