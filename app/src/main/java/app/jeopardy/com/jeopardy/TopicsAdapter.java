package app.jeopardy.com.jeopardy;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class TopicsAdapter extends Adapter<TopicsAdapter.MyViewHolder> {
    Context mcoContext;
    List<JSONObject> objlist = new ArrayList<>();
    boolean flag;

    public TopicsAdapter(Context context, List<JSONObject> objects,
                         boolean flag) {
        this.mcoContext = context;
        objlist = objects;
        this.flag = flag;
    }

    @Override
    public int getItemCount() {
        return objlist.size();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int i) {

        try {
            JSONObject obj = objlist.get(i);
            String name = obj.getString("Category");
            int resid = obj.getInt("Resource");
            holder.topic_name.setText(name); // Set text in textview
            // Load image into imageview
            Picasso.with(mcoContext).load(resid).into(holder.topic_image);

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        View view;

        if (flag) {
            view = LayoutInflater.from(mcoContext).inflate(
                    R.layout.landing_recycler_layout, arg0, false);
        } else {
            view = LayoutInflater.from(mcoContext).inflate(
                    R.layout.landing_recycler_layout, arg0, false);
        }
        return new MyViewHolder(view);
    }

    public JSONObject removeItem(int position) {
        JSONObject js = objlist.remove(position);
        notifyItemRemoved(position);
        return js;

    }

    public void addItem(int position, JSONObject obj) {
        objlist.add(position, obj);
        notifyItemInserted(position);

    }

    public void moveItem(int fromPosition, int toPosition) {
        JSONObject obj = objlist.remove(fromPosition);
        objlist.add(toPosition, obj);
        notifyItemMoved(fromPosition, toPosition);

    }

    public void animateTo(List<JSONObject> objects) {
        applyAndAnimateRemovals(objects);
        applyAndAnimateAdditions(objects);
        applyAndAnimateMovedItems(objects);
    }

    private void applyAndAnimateMovedItems(List<JSONObject> objects) {
        for (int toPosition = objects.size() - 1; toPosition >= 0; toPosition--) {
            final JSONObject model = objects.get(toPosition);
            final int fromPosition = objlist.indexOf(model);
            if (fromPosition >= 0 && fromPosition != toPosition) {
                moveItem(fromPosition, toPosition);
            }
        }

    }

    private void applyAndAnimateAdditions(List<JSONObject> objects) {
        for (int i = 0, count = objects.size(); i < count; i++) {
            final JSONObject model = objects.get(i);
            if (!objlist.contains(model)) {
                addItem(i, model);
            }
        }

    }

    private void applyAndAnimateRemovals(List<JSONObject> objects) {
        for (int i = objlist.size() - 1; i >= 0; i--) {
            final JSONObject model = objlist.get(i);
            if (!objects.contains(model)) {
                removeItem(i);
            }
        }
    }

    public class MyViewHolder extends
            android.support.v7.widget.RecyclerView.ViewHolder {
        ImageView topic_image;
        TextView topic_name;

        public MyViewHolder(View itemView) {
            super(itemView);
            topic_image = (ImageView) itemView
                    .findViewById(R.id.img_category_image);
            topic_name = (TextView) itemView
                    .findViewById(R.id.tv_topic_name);
        }

    }

}
