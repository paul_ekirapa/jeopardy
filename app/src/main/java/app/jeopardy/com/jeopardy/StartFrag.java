package app.jeopardy.com.jeopardy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.ScaleAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by hp on 7/26/2016.
 */
public class StartFrag extends Fragment implements View.OnClickListener {
    public StartFrag() {


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.start_game, container, false);
        Bundle bundle = getArguments();
        v.findViewById(R.id.coordinator).setAlpha(0.8f);
        TextView textView = (TextView) v.findViewById(R.id.tvstart);
        ImageView imageView = (ImageView) v.findViewById(R.id.imgStart);
        Picasso.with(getContext()).load(bundle.getInt("Resource")).into(imageView);
        textView.setText(bundle.getString("Topic"));
        Button start = (Button) v.findViewById(R.id.bt_start);
        start.setOnClickListener(this);
        setAnimation(start);
        return v;
    }

    private void setAnimation(final Button start) {
        final ScaleAnimation growAnim = new ScaleAnimation(1.0f, 1.05f, 1.0f, 1.05f);
        final ScaleAnimation shrinkAnim = new ScaleAnimation(1.05f, 1.0f, 1.05f, 1.0f);

        growAnim.setDuration(1500);
        shrinkAnim.setDuration(1500);

        start.setAnimation(growAnim);
        growAnim.start();

        growAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                start.setAnimation(shrinkAnim);
                shrinkAnim.start();
            }
        });
        shrinkAnim.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                start.setAnimation(growAnim);
                growAnim.start();
            }
        });
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.bt_start) {
            StartGame startGame = (StartGame) getActivity();
            startGame.startMyGame();
        }
    }

    public interface StartGame {
        void startMyGame();
    }
}
