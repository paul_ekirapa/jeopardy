package app.jeopardy.com.jeopardy;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Created by hp on 3/24/2016.
 */
public class Signin extends AppCompatActivity implements View.OnClickListener {
    private static final String PREF_NAME = "UserInfo";
    Button login, cancel;
    private EditText names, password;
    private SharedPreferences pref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin);
        pref = this.getSharedPreferences(PREF_NAME, MODE_PRIVATE);
        findViewById(R.id.btn_submit_login).setOnClickListener(this);
        findViewById(R.id.loginframe).setAlpha(0.6f);
        names = (EditText) findViewById(R.id.et_emailLogin);
        password = (EditText) findViewById(R.id.et_passwordLogin);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_submit_login) {
            if (check()) {
                boolean correct = new SQLiteHandler(getBaseContext()).getUser(names.getText().toString(), password.getText().toString());
                if (correct) {
//                    if (!pref.getString("Name", "0User").equals(names.getText().toString())) {
//                        new SQLiteHandler(getBaseContext()).createDB();
//                    }
                    Intent in = new Intent(getBaseContext(), MainActivity.class);
                    in.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    pref.edit().putString("Name", names.getText().toString()).apply();
                    pref.edit().putString("Level", "Beginner").apply();

                    SharedPreferences pref2 = this.getSharedPreferences("Created",
                            MODE_PRIVATE);
                    pref2.edit().putBoolean("Flag", true).apply();
                    startActivity(in);
                } else {
                    Toast.makeText(getBaseContext(), "Wrong Username and Password", Toast.LENGTH_LONG).show();
                    names.requestFocus();
                }
            }

        }
    }

    private boolean check() {
        if (names.getText().toString().isEmpty()) {
            names.setError(getString(R.string.error));
            names.requestFocus();
            return false;
        } else if (password.getText().toString().isEmpty()) {
            password.setError(getString(R.string.error));
            password.requestFocus();
            return false;
        }
        return true;

    }
}
