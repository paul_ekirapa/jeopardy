package app.jeopardy.com.jeopardy;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Adapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MyTopicsAdapter extends Adapter<MyTopicsAdapter.MyViewHolder> {
    Context mcoContext;
    List<String> list = new ArrayList<>();
    boolean flag;
    SQLiteHandler handler;

    public MyTopicsAdapter(Context context, List<String> list) {
        this.mcoContext = context;
        this.list = list;
        handler = new SQLiteHandler(context);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int i) {

            String name = list.get(i);
        Log.d("Category", name);
            int resid = handler.getCategoryResource(name);
            holder.topic_name.setText(name); // Set text in textview
            // Load image into imageview
            Picasso.with(mcoContext).load(resid).into(holder.topic_image);


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup arg0, int arg1) {
        View view;

        if (flag) {
            view = LayoutInflater.from(mcoContext).inflate(
                    R.layout.landing_recycler_layout, arg0, false);
        } else {
            view = LayoutInflater.from(mcoContext).inflate(
                    R.layout.landing_recycler_layout, arg0, false);
        }
        return new MyViewHolder(view);
    }

    public class MyViewHolder extends
            android.support.v7.widget.RecyclerView.ViewHolder {
        ImageView topic_image;
        TextView topic_name;

        public MyViewHolder(View itemView) {
            super(itemView);
            topic_image = (ImageView) itemView
                    .findViewById(R.id.img_category_image);
            topic_name = (TextView) itemView
                    .findViewById(R.id.tv_topic_name);
        }

    }

}
