package app.jeopardy.com.jeopardy;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;

import java.security.PrivateKey;

/**
 * Created by hp on 7/24/2016.
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    FragmentTransaction transaction;
    FrameLayout container;
    private Home home;
    private Topics topics;
    private Activity activity;
    private Feed feed;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        container = (FrameLayout) findViewById(R.id.main_frame);

        setToolbar();
        BottomMenu();
        home = new Home();
        topics = new Topics();
        feed = new Feed();
        activity = new Activity();

        transaction = getSupportFragmentManager().beginTransaction();
        if (savedInstanceState == null) {
            transaction.add(R.id.main_frame, home, "Home").commit();
        }
    }

    private void BottomMenu() {
        findViewById(R.id.menu1).setOnClickListener(this);
        findViewById(R.id.menu2).setOnClickListener(this);
        findViewById(R.id.menu3).setOnClickListener(this);
        findViewById(R.id.menu4).setOnClickListener(this);
    }

    private void setToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getString(R.string.app_name));
    }

    @Override
    public void onClick(View v) {
        transaction = getSupportFragmentManager().beginTransaction();
        switch (v.getId()) {
            case R.id.menu1:
                if(!home.isAdded()){
                    transaction.replace(R.id.main_frame,home).commit();
                }
                break;
            case R.id.menu2:
                if(!feed.isAdded()){
                    transaction.replace(R.id.main_frame,feed).commit();
                }
                break;
            case R.id.menu3:
                if(!topics.isAdded()){
                    transaction.replace(R.id.main_frame,topics).commit();
                }
                break;
            case R.id.menu4:
                if(!activity.isAdded()){
                    transaction.replace(R.id.main_frame,activity).commit();
                }
                break;
        }
    }
}
